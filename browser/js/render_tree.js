(function(window) {
  var document = window.document;

  window.renderTree = renderTree;

  function renderTree(container, tree, keyword) {
    keyword = keyword || '';

    var keywordLength = keyword.length;

    var ul = document.createElement('ul');

    generateTree(ul, tree);

    container.innerHTML = '';
    container.appendChild(ul);

    function generateTree(buffer, tree) {
      var li = document.createElement('li');
      var ul = document.createElement('ul');

      li.innerHTML = '<span>' + getVertexName(tree) + '</span>';
      li.appendChild(ul);

      var children = tree.children || [];

      children.forEach(function(child) {
        generateTree(ul, child);
      });

      buffer.appendChild(li);
    }

    function getVertexName(vertex) {
      var name = vertex.name || '';

      if (!vertex.matched) return name;

      var keywordStart = name.indexOf(keyword);
      var keywordEnd = keywordStart + keywordLength;

      return name.substring(0, keywordStart)
        + '<span class="hightlight">'
        + name.substring(keywordStart, keywordEnd)
        + '</span>'
        + name.substring(keywordEnd);
    }
  }
})(window);
