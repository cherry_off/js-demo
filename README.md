## Example of search and filter tree structure

- In `/browser` folder example of webpage with demo of filtering algorithm
  [demo link](https://nobazar.bitbucket.io/browser/)
- In `/console` folder simple filtering demo of console output
- In `/wip` folder lib for optimized tree filtering algorithm (not recursive),
  but it still in **Work In Progress**

## TODO:

- Change demo to optimized, non recursive search
- Add tests
